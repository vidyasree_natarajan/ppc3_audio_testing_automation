# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for audio testing automation of PPC3.

### Prerequisites ###

## Installation ##

* To use this framework you should install Protractor and Jasmine tools.

* Installing Protractor...

 Protractor is a Node.js program, to run protractor you need to install Node JS and JDK.

 If you use npm,

 npm install -g protractor

 (you can see the instructions to install protractor here http://www.protractortest.org/#/)

 Protractor needs two files to run. One is Configuration file and the other is spec file. We can run multiple spec files.

 Spec files have the test scripts to test the application.

 Conf file tells Protractor how to setup the chromedriver, which tests to run, how to setup the browsers, which framework to use.

 (If you want more informations about these two files. See https://github.com/angular/protractor/blob/master/docs/api-overview.md)

 (NOTE : 
 Make sure your protractor version is compatible with Node JS
 Protractor 5 is compatible with nodejs v6 and newer.
 )

* Installing Jasmine...

 If you use npm,

 npm install -g jasmine

 (you can follow the instructions to install jasmine here https://jasmine.github.io/pages/getting_started.html)

## Set up ##

* Clone this repository.

* This contains the files and folders like 

 * spec.js file

 Includes the test scripts to test the application
 
 * conf.js file

 This file contains the details to configure the protractor.

 File Structure :

 exports.config = {
 framework : <the framework which you have choosed>,
 specs : <This is an array which should contain all the spec files>,
 params : <global parameters should be mentioned as a property with its value>
 }
 (You can explore more about config file structure here https://github.com/angular/protractor/blob/master/lib/config.ts)
 ( NOTE : 
 Make sure the version of the chromeDriver is compatible with OS)
 )
 * config.json files

 The config.json should contain all the configurations of the application.

 JSON Structure:

 {
 "chromeDriverPath": ( path of chromedriver (required). If path is not specified it will throw an error ),
 "appToTest": ( app to test should be specified here ),
 "<appname>" " {
 "configs" : ( Array contains what are all the configurations to be tested )
 "audioModes: : {
 <configname> : ( Array contains what are all the process flows to be tested )
 }
 }
 "componentsToTest": ( It is an array to specify what are all the components to be tested ),
 "commonAudioInput": ( It is a path of the specific input audio file which is used to test the specified component ),
 "specificAudioInput": {
 <component_name> : "audio file path"
 }
 }

 (ex)

 {
 "chromeDriverPath": "C:/Program Files (x86)/Texas Instruments5825AT/PurePath Console 3/source/chromedriver",
 "appToTest": "TAS5825M",
 "componentsToTest": ["Input Mixer"],
 "commonAudioInput": "E:/PPC3_e2e/audio_files/input_audio/sinewave.wav",
 "specificAudioInput": {
 "Input Mixer": "E:/PPC3_e2e/audio_files/input_audio/sinewave.wav",
 "Clipper": "E:/PPC3_e2e/audio_files/input_audio/sinewave1.wav"
 }
 }
 
 ( NOTE : 
 * appToTest,chromeDriverPath and commonAudioInput are required fields.
 * If configs array is empty or undefined the test will be run for all the configurations.
 * If the audioModes array is empty or undefined the test will be run for all the process flows.
 * If the appname with configs and processflows is undefined it will run for all the configurations and process flows.
 * If the audio path for the specific component is not mentioned, it will take the common audio input
 )

 
 * ouput_files
 This folder contains the both output files .xlsx and JSON files after completion of the testing
 * audio_files
 This folder contains three audio files folders,
 input_audio - This folder includes all the input audio files
 reference_audio - This includes all the reference audio files 
 test_audio - This includes all the test audio files

 * components
 This folder includes all the component's spec files and testcases files.
 (ex)
 Input_Mixer (folder) - contains inputMixer.js and testCasesForInputMixer.json
 
 And also contains components.js file which requires all the components.
 
 * ppc3_files
 This includes the js file to unzip the ppc3 file

 unzipped (folder) - contains the unzipped ppc3 files
 zipped (folder) - contains the zipped ppc3 files


### How to run the test ? ###

* To run this test,
 (specify the chromedriverpath, application, configurations and processflows details in the config.json file)

 >> protractor conf.js

### Who do I talk to? ###
* Niranjan (niranjan.velraj@solitontech.com)
* Roshan (roshan.raju@solitontech.com)
* Karthikeswari (karthikeswari.veerakumar@solitontech.com)
* Vidyasree (vidyasree.natarajan@solitontech.com)
