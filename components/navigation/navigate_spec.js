var runInput = require('../../config.json');
var navigate_Utils = require('./navigate_Utils.js');
var path = require('path');
var until = protractor.ExpectedConditions; //This is to perform some protractor conditions
var processFlowPosition = {}; //This is to hold the process flow and its positions
var rootPath;
module.exports = {
    "navigateUpToTunningPage": function(configElement, pos, runInput) {
        navigate_Utils.chooseTheApp(rootPath, runInput);
        navigate_Utils.chooseTheConfiguration(configElement);
        navigate_Utils.chooseTheHomePageContent().then(function() {
            browser.wait(until.presenceOf(element.all(by.repeater('(key, value) in hybridFlowInfo'))), 10000, 'waiting');
            element.all(by.repeater('(key, value) in hybridFlowInfo')).each(function(selecthybridbtn, position) {
                var processFlowName = '';
                selecthybridbtn.all(by.css('.ng-binding')).each(function(paragraph) {
                    paragraph.getText().then(function(text) {
                        processFlowName += text + ' ';
                    });
                }).then(function() {
                    var obj = {};
                    processFlowName = processFlowName.substring(0, processFlowName.length - 1);
                    processFlowPosition[processFlowName] = position;
                });
            });
        });
    },
    "navigateThroughAudioModes": function(configElement, audioModeElement) {
        var defer = protractor.promise.defer();
        navigate_Utils.chooseTheProcessFlow(processFlowPosition, audioModeElement);
        navigate_Utils.savePPC3File(configElement).then(function() {
            browser.sleep(600);
            defer.fulfill();
        });
        return defer.promise;
    },
    "changeAudioMode": function(audioModeElement) {
        var defer = protractor.promise.defer();
        browser.sleep('1000');
        browser.wait(until.presenceOf(element(by.css('.aup-settings-ddown-holder'))), 5000, 'waiting');
        element(by.css('.aup-settings-ddown-holder')).click();
        browser.sleep('1000');
        browser.wait(until.presenceOf(element.all(by.css('.hflow-ddown-options')).get(3)), 5000, 'waiting');
        element.all(by.css('.hflow-ddown-options')).get(3).click().then(function() {
            defer.fulfill();
        });
        browser.sleep('1000');
        return defer.promise;
    },
    "navigateBackToHomePage": function() {
        var defer = protractor.promise.defer();
        var until = protractor.ExpectedConditions;
        browser.getCurrentUrl().then(function(url) {
            expect(path.basename(url)).toEqual('audioProcessing');
        });
        browser.wait(until.presenceOf(element(by.css('.popup-close'))), 10000, 'waiting');
        element(by.css('.popup-close')).click();
        browser.getCurrentUrl().then(function(url) {
            expect(path.basename(url)).toEqual('audioProcessing');
        });
        browser.wait(until.invisibilityOf(element(by.css('.popup-close'))), 10000, 'waiting');
        element.all(by.css('.breadscrum-link')).get(0).click().then(function() {
            browser.sleep('1000');
            browser.getCurrentUrl().then(function(url) {
                expect(path.basename(url)).toEqual('audioProcessing');
            });
            element.all(by.css('.imp-brj-btn.save-current')).get(1).click().then(function() {
                browser.getCurrentUrl().then(function(url) {
                    expect(path.basename(url)).toEqual('audioProcessing');
                });
            });

        });
        browser.getAllWindowHandles().then(function(handles) {
            browser.switchTo().window(handles[0]);
            browser.getCurrentUrl().then(function(url) {
                expect(path.basename(url)).toEqual('appcenter');
            });
            defer.fulfill();
        });
        return defer.promise;
    },
    "resolvePopup": function(path) {
        rootPath = path;
        var defer = protractor.promise.defer();
        browser.sleep(2000);
        browser.waitForAngularEnabled(false);
        var ele = element(by.css('.uncaughtException-popup-container'));
        ele.isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('.no-btn-uncaughtException')).click().then(function() {
                    defer.fulfill();
                });
            } else {
                defer.fulfill();
            }
        });
        return defer.promise;
    }
};