var until = protractor.ExpectedConditions;
var path = require('path');
var childprocess = require('child_process');
var rootPath;
module.exports = {
    "chooseTheApp": function(rootpath, runInput) {
        rootPath = rootpath;
        var defer = protractor.promise.defer();
        browser.getCurrentUrl().then(function(url) {
            expect(path.basename(url)).toEqual('appcenter');
        });
        browser.sleep(2000);
        browser.wait(until.presenceOf(element(by.id(runInput.appToTest)), 10000, 'waiting'));
        element(by.id(runInput.appToTest)).click();
        browser.getAllWindowHandles().then(function(handles) {
            browser.switchTo().window(handles[1]);
            defer.fulfill();
        });
        return defer.promise;
    },
    "chooseTheConfiguration": function(configElement) {
        var defer = protractor.promise.defer();
        browser.sleep(4000);
        var configIndex = -1;
        browser.wait(until.presenceOf(element(by.css('.nfp-start-btn.active'))), 10000, 'Element taking too long to appear in the DOM');
        element.all(by.css('.nfp-tile-device-name')).each(function(speaker, index) {
            speaker.getText().then(function(text) {
                if (text === configElement) {
                    currentConfig = configElement;
                    configIndex = index;
                }
            });
        }).then(function() {
            if (configIndex === -1) {
                console.log(configElement + " is not present in " + runInput.appToTest);
            } else {
                element.all(by.css('.nfp-tile-device-name')).get(configIndex).click();
                element(by.css('.nfp-start-btn.active')).click().then(function() {
                    browser.sleep(3000);
                    var popup = element(by.css('.custom-popup-skip-btns'));
                    popup.isDisplayed().then(function(displayed) {
                        if (displayed) {
                            popup.click();
                            browser.wait(until.invisibilityOf(popup), 10000, 'waiting to close');
                        }
                    });
                });
            }
            defer.fulfill();
        });
        return defer.promise;
    },
    "chooseTheHomePageContent": function() {
        var defer = protractor.promise.defer();
        browser.getCurrentUrl().then(function(url) {
            expect(path.basename(url)).toEqual('homepage');
        });
        var posit;
        browser.sleep(1000);
        browser.wait(until.presenceOf(element.all(by.repeater('blocks in rightBlocks.arrayValues'))), 10000, 'waiting');
        element.all(by.repeater('blocks in rightBlocks.arrayValues')).each(function(blocks, position) {
            element.all(by.css('.right-panel-blocks-heading')).get(position).getText().then(function(heading) {
                if (heading === "Tuning and Audio Processing") {
                    posit = position;
                }
            });
        }).then(function() {
            element.all(by.css('.right-panel-blocks-heading')).get(posit).click();
            browser.sleep(1000);
            browser.wait(until.invisibilityOf(element.all(by.css('.right-panel-blocks-heading')).get(posit)), 20000, 'waiting');
            defer.fulfill();
        });
        return defer.promise;
    },
    "chooseTheProcessFlow": function(processFlowPosition, audioModeElement) {
        var defer = protractor.promise.defer();
        var audioModePosition = processFlowPosition["" + audioModeElement + ""];
        browser.wait(until.presenceOf(element.all(by.css('.select-hybrid-btn')).get(audioModePosition)), 30000, 'waiting');
        element.all(by.css('.select-hybrid-btn')).get(audioModePosition).click();
        var evm_overlay;
        evm_overlay = element(by.css('.evm-pup-progress-container'));
        browser.wait(until.presenceOf(evm_overlay), 10000, 'waiting');
        browser.sleep('1000');
        browser.wait(until.invisibilityOf(evm_overlay), 10000, 'waiting').then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "savePPC3File": function(configElement) {
        var defer = protractor.promise.defer();
        var fileName;
        if (configElement.includes('/')) {
            var slashIndex = configElement.indexOf('/');
            fileName = configElement.substring(0, slashIndex) + configElement.substring(slashIndex + 1);
        } else {
            fileName = configElement;
        }
        var fs = require('fs');
        if (!fs.existsSync(rootPath + '/ppc3_files/zipped')) {
            fs.mkdirSync(rootPath + '/ppc3_files/zipped');
        }
        if (!fs.existsSync(rootPath + '/ppc3_files/unzipped')) {
            fs.mkdirSync(rootPath + '/ppc3_files/unzipped');
        }
        element(by.id('saveDBDialog')).sendKeys(rootPath + '/ppc3_files/zipped/' + fileName + '.ppc3').then(function() {
            browser.sleep(1500);
        }).then(function() {
            rootPath = rootPath.replace('\\', '/');
            var work = childprocess.spawnSync('node', ["" + rootPath + '/ppc3_files/unzip_zlib.js' + "", "" + rootPath + '/ppc3_files/zipped/' + fileName + '.ppc3' + "", "" + rootPath + '/ppc3_files/unzipped/' + fileName + '.json' + ""]);
            defer.fulfill();
        });
        return defer.promise;
    }
};