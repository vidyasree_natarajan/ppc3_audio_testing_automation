module.exports = {
    audio_player: require('./audio_player/audio_player_operations.js'),
    excel_generator: require('./excel_generation/excel_generator.js'),
    navigate: require('./navigation/navigate_spec.js')
};