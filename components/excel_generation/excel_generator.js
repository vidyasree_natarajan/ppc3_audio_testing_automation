module.exports = {
    "checkConsole": function() {
        var erroroccur = false;
        browser.manage().logs().get('browser').then(function(browserLog) {
            if (browserLog.length > 0) {
                browserLog.forEach(function(log) {
                    if (log.level.value > 900) {
                        browser.params.console.push(log.message);
                        erroroccur = true;
                    }
                });
            }
            if (!erroroccur) {
                browser.params.console.push('none');
            }
            var date = new Date();
            browser.params.time.push(date.getHours() + ' : ' + date.getMinutes() + ' : ' + date.getSeconds());
        });
    },
    "generateExcel": function(filename) {
        var fs = require('fs');
        var json2xls = require('json2xls');
        var json = require('../../output_files/jasmine-test-results.json');
        if (Object.keys(json).length !== 0) {
            var result = [];
            var noOfSuites = Object.keys(json);
            var xls, componentSuites = [];
            noOfSuites.forEach(function(suite) {
                if (json[suite].description.includes('Component Test')) {
                    componentSuites.push(suite);
                }
            });
            var consoleIndex = 0;
            componentSuites.forEach(function(componentsuite) {
                var suitedetail = json[componentsuite].description.split('-');
                json['' + componentsuite + ''].specs.forEach(function(ele, position) {
                    if (ele.fullName.includes('Component Test')) {
                        if (ele.status === 'failed') {
                            json['' + componentsuite + ''].specs[position].fail_message = json['' + componentsuite + ''].specs[position].failedExpectations[0].message;
                        } else {
                            json['' + componentsuite + ''].specs[position].fail_message = '--';
                        }
                        json['' + componentsuite + ''].specs[position].console_error = browser.params.console[consoleIndex];
                        json['' + componentsuite + ''].specs[position].suite = componentsuite;
                        json['' + componentsuite + ''].specs[position].time = browser.params.time[consoleIndex];
                        json['' + componentsuite + ''].specs[position].id = json['' + componentsuite + ''].specs[position].id.substring(4);
                        json['' + componentsuite + ''].specs[position].Config = suitedetail[2];
                        json['' + componentsuite + ''].specs[position].ProcessFlow = suitedetail[3];
                        json['' + componentsuite + ''].specs[position].Component = suitedetail[1];
                        consoleIndex++;
                        result.push(json[componentsuite].specs[position]);
                    }
                });
            });
            xls = json2xls(result, {
                fields: ['Config', 'ProcessFlow', 'Component', 'time', 'suite', 'id', 'fullName', 'status', 'fail_message', 'console_error']
            });
            fs.writeFileSync('./output_files/output_of_' + filename + '.xlsx', xls, 'binary');
            console.log('xlsx file ready!!!');
        }
    }
};