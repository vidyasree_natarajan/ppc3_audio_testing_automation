var until = protractor.ExpectedConditions;
module.exports = {
    "openAudioPlayer": function() {
        var defer = protractor.promise.defer();
        browser.sleep('500');
        var ele = element.all(by.css('.snaps-heading-holder')).get(2);
        ele.isDisplayed().then(function(displayed) {
            if (displayed) {
                ele.click().then(function() {
                    defer.fulfill();
                });
            }
        });
        return defer.promise;
    },
    "makeRecordButtonVisible": function() {
        var defer = protractor.promise.defer();
        browser.sleep(1000);
        browser.actions().dragAndDrop(element(by.css('.apl-tab-container')), { x: -20, y: 50 }).mouseUp().perform();
        var noOfClicks = [1, 2, 3, 4, 5, 6];
        noOfClicks.filter(function(ele) {
            element(by.css('li[title="Switch to audio player tab"]')).click();
            if (ele === 6) {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    "uploadAudio": function(audioFilePath) {
        var defer = protractor.promise.defer();
        browser.sleep(700);
        browser.wait(until.presenceOf(element(by.css('.apl-tab-container'))), 10000, "Element taking too long to appear");
        element.all(by.repeater('tracks in audioPlayerData.trackList')).each(function(tracks, trackPosition) {
            tracks.all(by.css('.apl-play-remove-btn')).get(trackPosition).click();
        });
        browser.sleep(500);
        element(by.css('input[name="WaveFile"]')).sendKeys(audioFilePath);
        browser.wait(until.presenceOf(element(by.css('.apl-play-loop-btn'))), 10000, "Element taking too long to appear");
        element(by.css('.apl-play-loop-btn')).click().then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "recordAudio": function() {
        var defer = protractor.promise.defer();
        browser.sleep(500);
        element(by.css('.apl-record-btn.ng-scope.stoprecording')).isDisplayed().then(function(displayed) {
            element(by.css('.apl-record-btn.ng-scope.stoprecording')).click().then(function() {
                defer.fulfill();
            });
        });
        return defer.promise;
    },
    "closeAudioPlayer": function() {
        var defer = protractor.promise.defer();
        browser.sleep(500);
        element(by.css('.snap-popup-container')).all(by.css('.snap-popup-close')).click().then(function() {
            browser.sleep('300')
            defer.fulfill();
        });
        return defer.promise;
    }
};