var fs = require('fs');
var abbr = require('./abbreviations.json');
module.exports = {
    "findTestCasesToRun": function(testInputsTestcases, testConfigJson, componentName) {
        var testCases = [];
        if (Object.keys(testConfigJson).includes("testCasesToRun")) {
            if (Object.keys(testConfigJson.testCasesToRun).includes(componentName)) {
                if (testConfigJson.testCasesToRun[componentName].length === 0) {
                    testCases = testInputsTestcases;
                } else {
                    testInputsTestcases.forEach(function(testCase) {
                        if (testConfigJson.testCasesToRun[componentName].includes(testCase.id)) {
                            testCases.push(testCase);
                        }
                    });
                }
            } else {
                testCases = testInputsTestcases;
            }
        } else {
            testCases = testInputsTestcases;
        }
        return testCases;
    },
    "switchDevice": function(device) {
        var defer = protractor.promise.defer();
        if (device === 'B') {
            element.all(by.id('deviceSelectControl')).get(0).all(by.css('.dumo-devices-holder')).get(0).click();
            defer.fulfill();
        }
        if (device === 'A') {
            element.all(by.id('deviceSelectControl')).get(0).all(by.css('.dumo-devices-holder')).get(1).click();
            defer.fulfill();
        }
        return defer.promise;
    },
    "recordAudio": function(audioPlayer, audio) {
        var defer = protractor.promise.defer();
        audioPlayer.openAudioPlayer().then(function() {
            audioPlayer.uploadAudio(audio).then(function() {
                audioPlayer.recordAudio().then(function() {
                    audioPlayer.closeAudioPlayer().then(function() {
                        defer.fulfill();
                    });
                });
            });
        });
        return defer.promise;
    },
    "dumpAudioFile": function(sourcePath, destPath, fileName, config, processFlow, deviceType, audioType) {
        var date = new Date();
        if (audioType === 'test_audio') {
            if (!fs.existsSync(destPath + "/audio_files/test_audio")) {
                fs.mkdirSync(destPath + "/audio_files/test_audio");
            }
            fs.createReadStream(sourcePath + '/node_modules/ti_addonmgr/node_modules/TAS5825M/sample.json').pipe(fs.createWriteStream('audio_files/test_audio/' + fileName + '_' + config.substr(6) + '_' + abbr.processFlows[processFlow] + '_' + deviceType + '_' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + '_' + date.getHours() + 'h' + '-' + date.getMinutes() + 'm' + '-' + date.getSeconds() + 's' + '.json'));
        } else if (audioType === 'reference_audio') {
            if (!fs.existsSync(destPath + "/audio_files/reference_audio")) {
                fs.mkdirSync(destPath + "/audio_files/reference_audio");
            }
            fs.createReadStream(sourcePath + '/node_modules/ti_addonmgr/node_modules/TAS5825M/sample.json').pipe(fs.createWriteStream('audio_files/reference_audio/' + fileName + '_' + deviceType + '_' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + '_' + date.getHours() + 'h' + '-' + date.getMinutes() + 'm' + '-' + date.getSeconds() + 's' + '.json'));
        }
    },
};