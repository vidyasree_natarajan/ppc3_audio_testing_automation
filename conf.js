var configuration_file_input = require('./config.json');
var excel_generator = require('./components/excel_generation/excel_generator.js');
exports.config = {};
exports.config.framework = 'jasmine';
exports.config.specs = ['spec.js'];
exports.config.chromeDriver = configuration_file_input.chromeDriverPath;
exports.config.params = {
    console: [],
    time: []
};
exports.config.jasmineNodeOpts = {
    defaultTimeoutInterval: 250000
};
exports.config.onPrepare = function() {
    var JSONReporter = require('jasmine-json-test-reporter');
    jasmine.getEnv().addReporter(new JSONReporter({
        file: './output_files/jasmine-test-results.json',
        beautify: true,
        indentationLevel: 4 // used if beautify === true
    }));
};
exports.config.onComplete = function() {
    excel_generator.generateExcel(configuration_file_input.appToTest);
};