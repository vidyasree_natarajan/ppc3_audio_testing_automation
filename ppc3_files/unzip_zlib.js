var zlib = require('zlib');
var fs = require('fs');

if(!process.argv[2]) {
	console.log('Please enter input file path');
	process.exit(-1);
}

if(!process.argv[3]) {
	console.log('Please enter output file path');
	process.exit(-1);
}

if(!zlib.inflateSync) {
	console.log('Newer version of nodejs is required to run this script');
	process.exit(-1);
}

if(!fs.existsSync(process.argv[2])) {
	console.log('Input file path doesnt exist');
	process.exit(-1);
}

try {
	fs.writeFileSync(process.argv[3], zlib.inflateSync(fs.readFileSync(process.argv[2])).toString());
} catch(exp) {
	console.log('Unknown error' + exp);
	process.exit(-1);
}

console.log('Done');
process.exit(0);
